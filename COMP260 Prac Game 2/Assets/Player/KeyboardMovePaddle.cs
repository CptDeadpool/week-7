﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
public class KeyboardMovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float force = 20f;
	public float speed = 20f;


	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}


	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");
		rigidbody.velocity = direction * speed;
	}
}
